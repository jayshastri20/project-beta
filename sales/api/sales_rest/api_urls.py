from django.urls import path
from sales_rest.api_views import api_list_sales, api_customer, api_sales_person, api_sales_record

urlpatterns = [
    path("sales/", api_list_sales, name="api_create_sale"),
    path("automobiles/<int:auto_vo_id>/sales/", api_list_sales, name="api_sales_detail"),
    path("sales/<int:pk>/", api_sales_record, name="api_sales_record"),
    path("sales/customer/", api_customer, name="api_customer"),
    path("sales/salesperson/", api_sales_person, name="api_sales_person"),
]