from common.json import ModelEncoder
from sales_rest.models import AutomobileVO, SalesPerson, Customer, SalesRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "number", "address" , "id"]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "price",
        "automobile",
        "salesman",
        "customer",
    ]
    encoders= {
        "automobile": AutomobileVOEncoder(),
        "salesman": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
