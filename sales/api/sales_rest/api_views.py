from django.http import JsonResponse
from sales_rest.models import AutomobileVO, SalesPerson, Customer, SalesRecord
from django.views.decorators.http import require_http_methods
from sales_rest.encoders import SalesPersonEncoder, CustomerEncoder, SalesRecordEncoder
import json


@require_http_methods(["GET", "POST"])
def api_sales_person(request):
    if request.method == "GET":
        salesman = SalesPerson.objects.all()
        return JsonResponse(
            {"salesman": salesman},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesman = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesman,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder= CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request, auto_vo_id=None):
    if request.method == "GET":
        if auto_vo_id is not None:
            sales = SalesRecord.objects.filter(automobile=auto_vo_id).select_related('salesman', 'customer', 'automobile')
        else:
            sales = SalesRecord.objects.all().select_related('salesman', 'customer', 'automobile')
        sales_list = [
            {
                "id": sale.id,
                "salesperson_name": sale.salesman.name,
                "employee_id": sale.salesman.employee_id,
                "customer_name": sale.customer.name,
                "automobile_vin": sale.automobile.vin,
                "price": sale.price,
            }
            for sale in sales
        ]
        return JsonResponse(
            {"sales": sales_list},
            encoder=SalesRecordEncoder,
        )
    
    else:
        content = json.loads(request.body)

        try:
            salesman = SalesPerson.objects.get(id=content["salesman"])
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid SalesPerson id"},
                status=400,
            )
        
        try:
            customer = Customer.objects.get(id=content["customer"])
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer id"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(id=content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile id"},
                status=400,
            )

        content["salesman"] = salesman
        content["customer"] = customer
        content["automobile"] = automobile
        
        new_sale = SalesRecord.objects.create(**content)
        return JsonResponse(
            {
                "salesperson_name": new_sale.salesman.name,
                "employee_id": new_sale.salesman.employee_id,
                "customer_name": new_sale.customer.name,
                "automobile_vin": new_sale.automobile.vin,
                "price": new_sale.price,
            },
            encoder=SalesRecordEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_sales_record(request, pk):
    if request.method == "GET":
        sales_record = SalesRecord.objects.filter(salesman=pk)
        sales_record_list = []
        for sales in sales_record:
            sales_record_list.append({
                "salesperson": sales.salesman.name,
                "customer": sales.customer.name,
                "automobile": sales.automobile.vin,
                "price": sales.price,
            })
        return JsonResponse(
            {"sales_record": sales_record_list},
            encoder=SalesRecordEncoder,
            safe=False,
        )