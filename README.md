# CarCar

Team:

Gabe Lugo - Automobile Service
Jay Shastri - Automobile Sales

## Design

Designed it fairly similar to the Wardrobe API and Conference GO API, we started by designing the backend with the 
needed models and fields to display what needs to be shown. We did not get much time to do styling yet so the web application
still has the same css features we were provided with.

## Service microservice

Created a AutomobileVO model, Technician Model, and ServiceAppointment Model with properties that Foreign Key 
to the other models respectively. I also made a get_automobiles poller to get data for my AutomobilesVO from the Inventory 
API's Automobile model. The encoders for AutomobileVO, Technician and ServiceAppointment were created before making the 
API views with the http required GET and POST request methods. There are views which lists and creates appointments, 
technician, and the service history of each automobile which I then added paths for in the urls files. Once all the models, views, and 
url paths were set and functioning, I moved on to the front-end of the project creating the required forms and lists that need 
to function or be displayed on the web application, adding the nav links to Nav.js and the routes in App.js. However, I could not get the 
finished or cancelled feature working in the list appointments section.

## Sales microservice

Created a AutomobileVO model, SalesPerson Model, Customer Model, and SalesRecord model with properties that Foreign Key 
to the other models respectively. I also made a get_automobiles poller to get data for my AutomobilesVO from the Inventory 
API's Automobile model. The encoders for AutomobileVO, Customer, SalesPerson and SalesRecord were created before making the 
API views with the http required GET and POST request methods. There are views which lists and creates customers, salesman, 
sales, and the sales records of each salesman which I then added paths for in the urls files. Once all the models, views, and 
url paths were set and functioning, I moved on to the front-end of the project creating the required forms and lists that need 
to function or be displayed on the web application, adding the nav links to Nav.js and the routes in App.js. 

