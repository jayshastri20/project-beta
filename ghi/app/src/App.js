import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SalesList from './SalesList';
import SalesHistoryList from './SalesHistoryList';
import ManufacturersList from './ManufacturersList';
import ManufacturersForm from './ManufacturersForm';
import VehiclesForm from './VehiclesForm';
import ModelsList from './VehiclesList';
import AutomobilesList from './AutomobileList';
import AutomobilesForm from './AutomobileForm';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointmentsList';
import ServiceHistory from './ServiceHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/salesman/add' element={<SalesPersonForm />} />
          <Route path='/customer/add' element={<CustomerForm />} />
          <Route path='/sales' element={<SalesList />} />
          <Route path='/sales/add' element={<SalesRecordForm />} />
          <Route path='/sales/history' element={<SalesHistoryList />} />
          <Route path='/manufacturers' element={<ManufacturersList />} />
          <Route path='/manufacturers/add' element={<ManufacturersForm />} />
          <Route path='/vehicles/add' element={<VehiclesForm />} />
          <Route path='/vehicles' element={<ModelsList />} />
          <Route path='/automobiles' element={<AutomobilesList />} />
          <Route path='/automobiles/add' element={<AutomobilesForm />} />
          <Route path='/service/add' element={<ServiceAppointmentForm />} />
          <Route path='/service' element={<ServiceAppointmentList />} />
          <Route path='/service/history' element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
