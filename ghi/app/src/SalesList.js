import { useEffect, useState } from "react";

function SalesList() {
  const [sales, setSales] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/sales_rest/sales/');
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  useEffect(() => {
    getData()
  }, []);



  return (
    <div>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesman</th>
            <th>Employee ID</th>
            <th>Automobile VIN</th>
            <th>Customer</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson_name}</td>
                <td>{sale.employee_id}</td>
                <td>{sale.automobile_vin}</td>
                <td>{sale.customer_name}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
