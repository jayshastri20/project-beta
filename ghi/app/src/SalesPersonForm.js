import React, { useState, useEffect } from "react";

function SalesPersonForm() {
    const [salesman, setSalesman] = useState([]);

    const [formData, setFormData] = useState({
        name: '',
        employee_id: '',
    })

    const fetchData = async () => {
        const url = "http://localhost:8090/sales_rest/sales/salesperson/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesman(data.salesman);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = `http://localhost:8090/sales_rest/sales/salesperson/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                employee_id: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Salesman</h1>
                    <form onSubmit={handleSubmit} id="create-salesman-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.employee_id} placeholder="Employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm;