import React, { useState, useEffect } from "react";

function VehiclesForm() {
    const [models, setModels] = useState([]);
    const [manufacturers, setManufacturers] = useState([]);
    const [isValidUrl, setIsValidUrl] = useState(false);

    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const fetchModels = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchManufacturers();
        fetchModels();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = `http://localhost:8100/api/models/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        if (inputName === "picture_url") {
            setIsValidUrl(false);
            const img = new Image();
            img.src = value;
            img.onload = () => setIsValidUrl(true);
            img.onerror = () => setIsValidUrl(false);
        }

        setFormData({
            ...formData,
            [inputName]: value
        });

    };

    const isDisabled = () => {
        return !isValidUrl || !formData.name || !formData.manufacturer_id;
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>New Vehicle</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture</label>
                            {!isValidUrl && formData.picture_url && <div className="text-danger">Invalid URL</div>}
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="manufacturer_id">Choose an Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        {isDisabled() && <div className="text-danger">Please fill out all required fields and provide a valid picture URL</div>}
                        <button className="btn btn-primary" disabled={isDisabled()}>Add Vehicle</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehiclesForm;