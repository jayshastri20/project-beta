import React, { useState, useEffect } from "react";

function ServiceAppointmentForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [technician, setTechnician] = useState([]);

    const [formData, setFormData] = useState({
        owner: '',
        appointment: '',
        reason: '',
        technician: '',
        automobile: '',

    })

    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    const fetchTechnicians = async () => {
        const url = "http://localhost:8080/service_rest/service/technician/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }
    }


    useEffect(() => {
        fetchAutomobiles();
        fetchTechnicians();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/service_rest/service/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                owner: '',
                appointment: '',
                reason: '',
                technician: '',
                automobile: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;


        setFormData({
            ...formData,
            [inputName]: value,
        });
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="automobile">Choose an Automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                                <option value="technician">Choose a Technician</option>
                                {technician.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>
                                            {technician.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.owner} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control" />
                            <label htmlFor="owner">Owner</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.appointment} placeholder="Appointment" required type="datetime-local" name="appointment" id="appointment" className="form-control" />
                            <label htmlFor="appointment">Select Date and Time</label>
                            {formData.appointment && (
                                <p>Formatted Date: {new Date(formData.appointment).toLocaleString()}</p>
                            )}
                        </div>
                        <button className="btn btn-primary">Book Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ServiceAppointmentForm;