import { useEffect, useState } from "react";

function ServiceAppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/service_rest/service/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const cancelAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/service_rest/service/${id}`, {
            method: 'DELETE'
        });
        if (response.ok) {
            // Remove the appointment from the state
            const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
            setAppointments(updatedAppointments);
        } else {
            console.error(`Failed to cancel appointment with id ${id}`);
        }
    };

    const finishAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/service_rest/service/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                finished: true
            })
        });
        if (response.ok) {
            // Update the appointment in the state
            const updatedAppointments = appointments.map(appointment => {
                if (appointment.id === id) {
                    return {
                        ...appointment,
                        finished: true
                    };
                } else {
                    return appointment;
                }
            });
            setAppointments(updatedAppointments);
        } else {
            console.error(`Failed to finish appointment with id ${id}`);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date and Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.automobile}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.appointment).toLocaleString()}</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    {appointment.finished ? (
                                        <button className="btn btn-secondary disabled">Finished</button>
                                    ) : (
                                        <>
                                            <button onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">Cancel</button>
                                            <button onClick={() => finishAppointment(appointment.id)} className="btn btn-success">Finish</button>
                                        </>
                                    )}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceAppointmentList;
