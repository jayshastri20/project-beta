import React, { useState, useEffect } from "react";

function SalesHistoryList() {
    const [salesman, setSalesman] = useState([]);
    const [selectedSalesman, setSelectedSalesman] = useState("");
    const [sales, setSales] = useState([]);

    useEffect(() => {
        fetchSalesman();
    }, []);

    const fetchSalesman = async () => {
        const url = "http://localhost:8090/sales_rest/sales/salesperson/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesman(data.salesman);
        }
    };

    const fetchSales = async () => {
        const url = `http://localhost:8090/sales_rest/sales/${selectedSalesman}/`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales_record);
        }
    };

    const handleSalesmanChange = (event) => {
        const salesmanId = event.target.value;
        setSelectedSalesman(salesmanId);
    };

    useEffect(() => {
        if (selectedSalesman !== "") {
            fetchSales();
        }
    }, [selectedSalesman]);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Sales History List</h1>
                    <div className="mb-3">
                        <select
                            onChange={handleSalesmanChange}
                            value={selectedSalesman}
                            required
                            name="salesPerson"
                            id="salesPerson"
                            className="form-select"
                        >
                            <option value="">Choose a Salesman</option>
                            {salesman.map((salesman) => {
                                return (
                                    <option key={salesman.id} value={salesman.id}>
                                        {salesman.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    {selectedSalesman !== "" && (
                        <div>
                            <h2>Recorded Sales</h2>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Salesman</th>
                                        <th>Customer</th>
                                        <th>VIN</th>
                                        <th>Sale Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {sales.map((sale, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{sale.salesperson}</td>
                                                <td>{sale.customer}</td>
                                                <td>{sale.automobile}</td>
                                                <td>{sale.price}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}

export default SalesHistoryList;