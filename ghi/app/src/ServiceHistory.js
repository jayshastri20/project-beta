import React, { useState, useEffect } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [selectedAutomobile, setSelectedAutomobile] = useState("");
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        fetchAutomobiles();
    }, []);

    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    };

    const fetchServiceHistory = async () => {
        const url = `http://localhost:8080/service_rest/service/${selectedAutomobile}/`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.service_history);
        }
    };

    const handleAutomobileChange = (event) => {
        const selectedID = event.target.value;
        setSelectedAutomobile(selectedID);
    };

    useEffect(() => {
        if (selectedAutomobile !== "") {
            fetchServiceHistory();
        }
    }, [selectedAutomobile]);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Service Appointment History</h1>
                    <div className="mb-3">
                        <select onChange={handleAutomobileChange} value={selectedAutomobile} required name="vin" id="vin" className="form-select">
                            <option value="">Choose an Automobile</option>
                            {automobiles.map((auto) => {
                                return (
                                    <option key={auto.id} value={auto.id}>
                                        {auto.vin}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    {selectedAutomobile !== "" && (
                        <div>
                            <h2>Service History</h2>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>VIN</th>
                                        <th>Customer Name</th>
                                        <th>Date</th>
                                        <th>Technician</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {appointments.map((appointment, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{appointment.vin}</td>
                                                <td>{appointment.customer}</td>
                                                <td>{new Date(appointment.appointment).toLocaleString()}</td>
                                                <td>{appointment.technician}</td>
                                                <td>{appointment.reason}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}

export default ServiceHistory;