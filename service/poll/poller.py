import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


from service_rest.models import AutomobileVO


def get_automobiles():
    print("Get Automobiles function running")
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for vehicle in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=vehicle["href"],
            defaults={
                "vin": vehicle["vin"],
            },
        )
    print("This is AutomobileVO objects", AutomobileVO.objects.all())


def poll():
    while True:
        print("Service poller polling for data")
        try:
            get_automobiles()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
