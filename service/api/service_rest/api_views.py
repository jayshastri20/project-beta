from django.http import JsonResponse
from service_rest.models import AutomobileVO, Technician, ServiceAppointment
from django.views.decorators.http import require_http_methods
from service_rest.encoders import TechnicianEncoder, ServiceAppointmentEncoder
import json


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, auto_vo_id=None):
    if request.method == "GET":
        if auto_vo_id is not None:
            appointments = ServiceAppointment.objects.filter(
                automobile=auto_vo_id
            ).select_related("technician", "automobile")
        else:
            appointments = ServiceAppointment.objects.all().select_related(
                "technician", "automobile"
            )
        appointment_list = [
            {
                "id": a.id,
                "automobile": a.automobile.vin,
                "customer": a.owner,
                "appointment": a.appointment,
                "technician": a.technician.name,
                "reason": a.reason,
                "status": a.status,
            }
            for a in appointments
        ]

        return JsonResponse(
            {"appointments": appointment_list},
            encoder=ServiceAppointmentEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(id=content["technician"])
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "invalid technician id"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid automobile id"},
                status=400,
            )

        content["technician"] = technician
        content["automobile"] = automobile

        new_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            {
                "automobile": new_appointment.automobile.vin,
                "owner": new_appointment.owner,
                "appointment": new_appointment.appointment,
                "technician": new_appointment.technician.name,
                "reason": new_appointment.reason,
                "status": new_appointment.status,
            },
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT"])
def api_service_history(request, pk):
    if request.method == "GET":
        service_history = ServiceAppointment.objects.filter(automobile=pk)
        service_history_list = []
        for service in service_history:
            service_history_list.append(
                {
                    "vin": service.automobile.vin,
                    "customer": service.owner,
                    "appointment": service.appointment,
                    "technician": service.technician.name,
                    "reason": service.reason,
                    "status": service.status,
                }
            )
        return JsonResponse(
            {"service_history": service_history_list},
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        # Update the status to finished or cancelled
        status = request.POST.get("status", "")
        if status == "finished":
            service.status = ServiceAppointment.objects.filter(status).FINISHED
        elif status == "cancelled":
            service.status = ServiceAppointment.objects.filter(status).CANCELLED
        else:
            return JsonResponse({"error": "Invalid status"}, status=400)

        service.save()
        data = {
            "id": service.id,
            "automobile": service.automobile.vin,
            "customer": service.owner,
            "appointment": service.appointment,
            "technician": service.technician.name,
            "reason": service.reason,
            "status": service.status,
        }
        return JsonResponse({"service": data})

    elif request.method == "DELETE":
        # Delete the service appointment
        service.delete()
        return JsonResponse({"success": True})
