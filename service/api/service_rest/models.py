from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=6, unique=True)


class ServiceAppointment(models.Model):
    owner = models.CharField(max_length=100)
    appointment = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.BooleanField(null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="services",
        on_delete=models.CASCADE,
    )
