# Generated by Django 4.0.3 on 2023-03-10 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_rename_vehicle_serviceappointment_automobile'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceappointment',
            name='status',
            field=models.BooleanField(null=True),
        ),
    ]
