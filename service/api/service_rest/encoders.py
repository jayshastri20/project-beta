from common.json import ModelEncoder
from service_rest.models import AutomobileVO, Technician, ServiceAppointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number", "id"]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
       "owner",
       "appointment",
       "reason",
       "status",
    ]
    encoders= {
        "automobile": AutomobileVOEncoder(),
        "technician": TechnicianEncoder(),

    }
