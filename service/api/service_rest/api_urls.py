from django.urls import path
from service_rest.api_views import (
    api_service_history,
    api_list_appointments,
    api_technician,
)


urlpatterns = [
    path("service/", api_list_appointments, name="api_create_appointment"),
    path(
        "automobiles/<int:auto_vo_id>/service/",
        api_service_history,
        name="api_service_detail",
    ),
    path("service/technician/", api_technician, name="api_technician"),
    path("service/<int:pk>/", api_service_history, name="api_service_history"),
]
